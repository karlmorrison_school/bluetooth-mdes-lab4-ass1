#include <SoftwareSerial.h>
int bluetoothTx = 10; // TX-O pin 
int bluetoothRx = 11; // RX-I pin
SoftwareSerial bluetooth(bluetoothTx, bluetoothRx); // name of serial "bluetooth" with what pins
void setup() {
  Serial.begin(9600); // Open serial communications and wait for port to open:
  bluetooth.begin(115200); // set the data rate for the SoftwareSerial port
  
  // the 3 dollar things puts the print into command mode
  bluetooth.print("$"); // print dollar character, send to bluetooth, page 6 rn_bluetooth_um.pdf "NOTE: You can only enter command mode remotely over Bluetooth if you have made a connection and sent the $$$ within the “config timer” window after powerup"
  bluetooth.print("$"); 
  bluetooth.print("$"); 
  
  //Serial.println("Goodnight moon!"); // send to Serial port tex COM4
  //bluetooth.print("loolololololwz"); 
  
  delay(100); // delay 100 milliseconds
  bluetooth.println("U,9600,N"); 
  bluetooth.begin(9600); // set sepeed (baud rate)
}

void loop() { // run over and over
  if(bluetooth.available()) { 
    Serial.print((char)bluetooth.read()); 
  }
  if(Serial.available()) { 
    bluetooth.print((char)Serial.read()); 
  } 
}

// bluetooth pairing != connected

package com.example.mypackage;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.UUID;

import com.example.mypackage.FragmentMiddle.OnButtonToggleListener;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MainActivity extends Activity implements OnButtonToggleListener {

	private final static int REQUEST_ENABLE_BT = 1234; // Detta m�ste vara ett, i din app, unikt heltal
	private BluetoothAdapter mBluetoothAdapter;
	private BluetoothSocket mBluetoothSocket;
	private static final UUID SPP_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
	private static final String mac = "00:06:66:64:43:B9";
	private ConnectedThread mConnectedThread;
	private ConnectThread mConnectThread;
	FragmentMiddle fragMiddle;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		FragmentManager fm = this.getFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		View fc = findViewById(R.id.fragment_container);

		fragMiddle = new FragmentMiddle();

		ft.replace(fc.getId(), fragMiddle);
		ft.addToBackStack(null);
		ft.commit();
	}
	
	@Override
	protected void onStart() {
		if(!hasBluetoothSupport())
			Toast.makeText(this, "bluetooth not supported", Toast.LENGTH_SHORT).show();
		else
			ensureBluetoothEnabled();	
		super.onStart();
	}

	private boolean hasBluetoothSupport() { 
		mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();		
		return (mBluetoothAdapter != null); 
	}

	private void ensureBluetoothEnabled() { 
		if (!mBluetoothAdapter.isEnabled()) {
			Intent enableBtIntent = new Intent( BluetoothAdapter.ACTION_REQUEST_ENABLE); 
			startActivityForResult(enableBtIntent, REQUEST_ENABLE_BT); 
		} else { 
			connectToBluetoothDevice(mac);
		}
	}

	@Override 
	protected void onActivityResult(int requestCode, int resultCode, Intent data) { 
		if (requestCode == REQUEST_ENABLE_BT) { 
			if (resultCode == RESULT_OK) { 
				connectToBluetoothDevice(mac);
			} else { 
				Toast.makeText(this, "vad h�ller du p� med, sl� p� skiten", Toast.LENGTH_SHORT).show();
			}
		}
	}

	private void connectToBluetoothDevice(String mac) {
		Toast.makeText(this, "connect", Toast.LENGTH_SHORT).show();
		if (BluetoothAdapter.checkBluetoothAddress(mac)) { 
			BluetoothDevice mBluetoothDevice = mBluetoothAdapter.getRemoteDevice(mac); 
			try {
				mBluetoothSocket = mBluetoothDevice.createRfcommSocketToServiceRecord(SPP_UUID);
				mConnectThread = new ConnectThread(mBluetoothDevice);
				mConnectThread.start();
			} catch (IOException e) {
				e.printStackTrace();				
			}
		}
	}


	private class ConnectThread extends Thread { 
		private final BluetoothSocket mmSocket; 
		private final BluetoothDevice mmDevice;
 
		public ConnectThread(BluetoothDevice device) { 
			BluetoothSocket tmp = null; 
			mmDevice = device; 
			try { 
				tmp = device.createRfcommSocketToServiceRecord(SPP_UUID); 
			} catch (IOException e) { 
			} 
			mmSocket = tmp; 
		} 
		public void run() { 
			try { 
				mmSocket.connect(); 
			} catch (IOException connectException) { 
				try { 
					mmSocket.close(); 
				} catch (IOException closeException) { 
				} 
				return; 
			} 
			mConnectedThread = new ConnectedThread(mmSocket);
			mConnectedThread.start();
		} 
		public void cancel() { 
			try { 
				mmSocket.close(); 
			} catch (IOException e) { 
			} 
		} 
	}

	private class ConnectedThread extends Thread { 
		private final BluetoothSocket mmSocket; 
		private final InputStream mmInStream; 
		private final OutputStream mmOutStream; 
		public ConnectedThread(BluetoothSocket socket) { 
			mmSocket = socket; 
			InputStream tmpIn = null; 
			OutputStream tmpOut = null; 
			try { 
				tmpIn = socket.getInputStream(); 
				tmpOut = socket.getOutputStream(); 
			} catch (IOException e) { 
			} 
			mmInStream = tmpIn; 
			mmOutStream = tmpOut; 
		} 

		public void run() { 
			byte[] buffer = new byte[1024]; 
			int bytes; 
			while (true) { 
				try { 
					bytes = mmInStream.read(buffer); 
					if( bytes > 0) 
						Log.i("lol", "Got value " + buffer[0]); 
						
						final byte bufferFinal = buffer[0];
						
						runOnUiThread(new Runnable() {
							@Override
							public void run() {
								fragMiddle.progressBar.setProgress( bufferFinal);
								fragMiddle.progressText.setText(bufferFinal+"/"+fragMiddle.progressBar.getMax());
							}
						});
					

				} catch (IOException e) { 
					break; 
				} 
			} 
		} 

		public void write(byte[] bytes) { 
			try { 
				mmOutStream.write(bytes); 
			} catch (IOException e) { 
			} 
		} 

		public void cancel() { 
			try { 
				mmSocket.close(); 
			} catch (IOException e) { 
			} 
		} 
	}

	private synchronized void stop() { 
		if (mConnectThread != null) { 
			mConnectThread.cancel(); 
			mConnectThread = null; 
		} 
		if (mConnectedThread != null) { 
			mConnectedThread.cancel(); 
			mConnectedThread = null;
		} 
	}

	@Override
	public void onButtonToggleListener(Boolean checked) {
		if(!checked)
			stop();
		else
			ensureBluetoothEnabled();
	}
}

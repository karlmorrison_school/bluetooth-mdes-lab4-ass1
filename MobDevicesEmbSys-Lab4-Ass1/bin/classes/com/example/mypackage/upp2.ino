long lastTime, interval = 50; 
int potentiometer = A0, led = 13; 
byte incomingByte; void setup() { 
  Serial1.begin(115200); 
  pinMode(led, OUTPUT); 
  digitalWrite(led, LOW); 
  lastTime = millis(); 
}

void loop() { 
  if ((millis() - lastTime) > interval) { 
    int data = analogRead(potentiometer); 
    data = map(data, 0, 1023, 0, 100); 
    Serial1.write(data); lastTime = millis(); 
  }
  if (Serial1.available() > 0) { 
    incomingByte = Serial1.read(); 
    if (incomingByte == 'H') { 
      digitalWrite(led, HIGH); } else if (incomingByte == 'L') { digitalWrite(led, LOW);
    } 
  } 
}
